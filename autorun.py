#!/usr/bin/python2

#fancy multi-threaded module opener!
from subprocess import Popen
from collections import OrderedDict

import os
import time
from make import yesno

def autorun():
    title = '''
        ==============================
           
         katzmaus utility tool script 
         
        ==============================                           
       
----------------------------------------
 options: (all true recommended)
----------------------------------------
        '''
    print(title)
    
    choices = dict()
    
    #general utilities
    choices['gazebo']   = yesno('Launch gazebo? ')
    choices['rqtgraph'] = yesno('Launch rqt_graph? ')
    
    #camera specific
    choices['vision'] = yesno('Launch vision module? ')
    choices['rqtimg'] = yesno('Launch rqt image viewer? ')

    #motion specific
    choices['collision'] = yesno('Launch collision avoidance? ') 
    choices['homing']    = yesno('Launch relative homing? ') 
    
    
    #TODO: later on game logic
    #choices['actions'] = yesno('Launch action controller? ')
       

    #Use process dictonary to keep track of all processes
    p = OrderedDict()
    
        
    ##Run ROS core
    p['roscore'] = Popen('roscore')
    time.sleep(1)
    
    #Run Gazebo
    if choices['gazebo']:
        print('\n>> Opening gazebo.')
        p['gazebo'] = Popen(['roslaunch', 'src/p3dx/p3dx_gazebo/launch/gazebo.launch'])
    
    #Run RQT Graph
    if choices['rqtgraph']:
        print('\n>> Opening rqt graph.')
        p['rqtgraph'] = Popen(['rosrun', 'rqt_graph', 'rqt_graph'])
    
    #Run Vision Module
    if choices['vision']:
        print('\n>> Running vision module.')
        p['vision'] = Popen(['rosrun', 'katzmaus', 'vision.py'])
    
    #Run RQT Image viewer
    if choices['rqtimg']:
        print('\n>> Opening rqt image viewer.')
        p['rqtimg'] = Popen(['rosrun', 'rqt_image_view', 'rqt_image_view'])
    
    #Run collision avoidance module
    if choices['collision']:
        print('\n>> Opening collision avoidance module.')
        p['collision'] = Popen(['rosrun', 'katzmaus', 'collision_homing.py'])
    
    #Homing consits of move_to and relative_move to
    if choices['homing']:
        print('\n>> Opening homing modules.')
        p['moveto'] = Popen(['rosrun', 'katzmaus', 'homing.py'])
        p['rel_moveto'] = Popen(['rosrun', 'katzmaus', 'relative_homing_interface.py'])
    
    #Main Loop
    p['roscore'].wait()
    
    #Clean up all the dirty stuff
    for p_name in reversed(p):
        p[p_name].terminate()
        

if __name__=='__main__':
    autorun()
