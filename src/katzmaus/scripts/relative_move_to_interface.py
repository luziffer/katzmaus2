#!/usr/bin/env python

import os

import rospy
import math
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64MultiArray
from nav_msgs.msg import Odometry
from geometry_msgs.msg._Vector3 import Vector3
from operation_mode import op_mode

class Control:
    def __init__(self):
        rospy.init_node("relative_move_to_interface", anonymous = True)
        #absolute publishing
        self.abs = rospy.Publisher('move_to_command', Vector3, queue_size = 10)
        #relative subscribing
        self.rel = rospy.Subscriber('rel_move_to_command', Vector3, self.convert_to_abs)
        
        self.absPose = rospy.Subscriber('abs_pose', Vector3, self.read_pose)
        
        rate = rospy.Rate(1)

        rospy.spin()
        #while not rospy.is_shutdown():
            #rdist = float(raw_input("relative Target dist? "))
            #ryaw = float(raw_input("relative Target yaw? "))
            #self.convert_to_abs(Vector3(rdist,0.0,ryaw))
            
    def convert_to_abs(self, rel):
        ryaw = rel.z
        rdist = rel.x
        
        tyaw = ryaw + self.pyaw
        dx = rdist* math.sin(math.radians(tyaw))
        dy = -rdist*math.cos(math.radians(tyaw))
        tx = self.px + dx
        ty = self.py + dy
        self.abs.publish(Vector3(tx, ty, tyaw))
        
    def read_pose(self, data):
        self.px = data.x
        self.py = data.y
        self.pyaw = data.z 

c = Control()


