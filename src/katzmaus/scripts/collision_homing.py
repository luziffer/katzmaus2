#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Node, das Kollisionen verhindern soll. Bekommt (momentan) Keyboard Input von keyboard_interface. Der wird zu p3dx/cmd_vel weitergeleitet,
es sei denn Laser erkennt eine zukünftige Kollision. Dann wird Keyboard Input abgeschnitten und es wird eine Korrekturbewegung eingeleitet:
Zuerst wird rückwärts gefahren und anschließend in die Richtung gedreht, in der das Hindernis sich NICHT befindet. Danach wird der Keyboard
Input wieder freigegeben. Die Laser simulieren echte Inputs, indem nur 8 Laser Daten ausgewertet werden. Um eine Kollision zu berechnen,
wird die Durchschnittsintensität der letzten 10 Inputs berechnet, ist diese kleiner als ein Threshold wird die Kollisionsphase eingeleitet.
"""

import rospy
from katzmaus.msg import Surrounding
from std_msgs.msg import String, Int16
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import PointCloud
#necessary when creating own velocity data for correction turn
from geometry_msgs.msg._Vector3 import Vector3
from operation_mode import op_mode
from collections import deque
import numpy as np

cmd_vel_path_output = ""
cmd_vel_path_input  = ""
laser_path = ""
if op_mode:
    #experiment:
    cmd_vel_path_output = "RosAria/cmd_vel"
    cmd_vel_path_input  = "move_to_cmd_vel"
    laser_path          = "RosAria/sonar" 
else:
    cmd_vel_path_output = 'p3dx/p3dx_velocity_controller/cmd_vel'
    cmd_vel_path_input  = "move_to_cmd_vel"
    laser_path          = 'p3dx/p3dx/laser/scan'

class Laser:
    def __init__(self, collision):
        self.real_input = np.ones(8)
        self.history = deque()
        self.history_length = 8
        for k in range(self.history_length):
            self.history.append(np.ones(8))
        self.average = np.ones(8)
        self.min = 1
        self.min_index = 0
        self.collision = collision
        
    def printData(self, data, descr = ""):
        """ 
        data: numpy array of size 8, the intensities of the laser that should be printed
        """
        l = data
        result = descr + ": [{:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}]".format(l[0], l[1], l[2], l[3], l[4], l[5], l[6], l[7])
        rospy.loginfo(result)
        
    def checkForCollision(self, threshold):
        """
        returns true if there is an object in the way!
        """
        #check for the 6 middle sensors
        self.min = np.min(self.average[1:7])
        if self.min < threshold:
            return True
        else:
            return False
           
    def getLeftValue(self):
        return np.mean(self.average[4:8])
    
    def getRightValue(self):
        return np.mean(self.average[0:4])
    
    def callback(self, data):
        if not op_mode:
            #simulation
            d = data.ranges	
            #get raw data
            #the the first entry is the sensity of the most right laser, the last from the most left
            self.real_input = np.array([d[10], d[50], d[100], d[170], d[190],d[260], d[310],d[350]])
        else:
            for i in range(8):
                x,y = data.points[i].x, data.points[i].y
                norm = np.sqrt(x*x + y*y)
                self.real_input[i] = norm
        #self.real_input ist jetzt der aktuelle Input vom Sonar
        #create small history of the last 10 inputs
        self.history.append(self.real_input)
        self.history.popleft()
        #calculate average over history
        self.average = sum(self.history) / len(self.history)
        pathBlocked = self.checkForCollision(1.0)
        if pathBlocked:
            self.collision.state = 1
            
        
       


class Collision:

    def __init__(self):
        # In ROS, nodes are uniquely named. If two nodes with the same
        # name are launched, the previous one is kicked off. The
        # anonymous=True flag means that rospy will choose a unique
        # name for our 'listener' node so that multiple listeners can
        # run simultaneously.
        rospy.init_node('collision', anonymous=True)
        self.pub = rospy.Publisher(cmd_vel_path_output, Twist, queue_size = 10)
        
        self.state = 0
        #state = 0: normal mode, accepts Keyboard input or move_to input
        #state = 1: collision Mode, keyboard input or move_to input nicht akzeptiert
        
        self.t = 0
        self.laser = Laser(self)
        
        self.direction_checked = False
        self.left = 0.0
        self.right = 0.0
        if op_mode:
            #experiment
            rospy.Subscriber(laser_path, PointCloud, self.laser.callback)
        else:
            #simulation
            rospy.Subscriber(laser_path, LaserScan, self.laser.callback)
        rospy.Subscriber(cmd_vel_path_input, Twist, self.callback_move_to)
        
        #vision
        self.enabled = True
        self.yaw_threshold = 50
        rospy.Subscriber("objects", Surrounding, self.callback_vision)
        
        self.output = rospy.Publisher('collision_output', Int16, queue_size = 10)
        
        rate = rospy.Rate(25) # 25hz
        while not rospy.is_shutdown():
            #rospy.loginfo(str(self.state))
            if self.state == 0:
                self.output.publish(0)
                self.t = 0
            if self.state == 1 and self.enabled == True:
                self.output.publish(1)
                if not self.direction_checked:
                    #todo:check if left or right is free
                    self.left = self.laser.getLeftValue()
                    self.right = self.laser.getRightValue()    
                    self.direction_checked = True
                    
                if self.left < self.right:
                    #turn right
                    if self.laser.checkForCollision(2.0):
                        #go forward if there is space
                        vel_cmd = Twist(Vector3(0.0,0.0,0.0), Vector3(0.0,0.0,0.5))
                    else:
                        vel_cmd = Twist(Vector3(0.5,0.0,0.0), Vector3(0.0,0.0,0.5))
                else:
                    #turn left
                    if self.laser.checkForCollision(2.0):
                        #go forward if there is space
                        vel_cmd = Twist(Vector3(0.0,0.0,0.0), Vector3(0.0,0.0,-0.5))
                    else:
                        vel_cmd = Twist(Vector3(0.5,0.0,0.0), Vector3(0.0,0.0,-0.5))
                
                self.pub.publish(vel_cmd)
                self.t += 1
            #update Modes
            if self.t >= 10:
                if self.laser.checkForCollision(1.0):
                    self.state = 1
                    self.t = 0
                else:
                    self.state = 0
                    self.t = 0
                    self.direction_checked = False
            rate.sleep()
            
        # spin() simply keeps python from exiting until this node is stopped        
        rospy.spin()

    
        
    
    def callback_move_to(self, data):
        #only pass the keyboard data if we are in normal mode.
        if self.state == 0 or (self.state == 1 and self.enabled == False): 
            self.pub.publish(data)
         
    def callback_vision(self, obj):
        pass
#        rospy.loginfo(self.enabled)
#        curtain_string = None
#        if "blue_curtain" in obj.objects:
#            curtain_string = "blue_curtain"
#        if "orange_curtain" in obj.objects:
#            curtain_string = "orange_curtain"
#        if curtain_string is None:
#            self.enabled = True
#        else:
#            check for distance
#            curtain_id = obj.objects.index(curtain_string)
#            curtain_tri = obj.triangles[curtain_id]
#            if curtain_tri.yaw_min <= 0  and curtain_tri.yaw_max >= 0:
#                self.enabled = False
#            else:
#                self.enabled = True
                
    


if __name__ == '__main__':
    rospy.loginfo(op_mode)
    c = Collision()
