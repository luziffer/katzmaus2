#!/usr/bin/env python

import os

import rospy
import math
import random
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64MultiArray, String, Int16
from nav_msgs.msg import Odometry
from geometry_msgs.msg._Vector3 import Vector3
from operation_mode import op_mode
from katzmaus.msg import Surrounding

class Search:
    def __init__(self):
        self.state = 0
        rospy.init_node('search', anonymous=True)
        self.output = rospy.Publisher('finished', Int16, queue_size = 10)
        self.rel = rospy.Publisher('rel_homing_command', Vector3, queue_size = 10)
        self.absPose = rospy.Subscriber('abs_pose', Vector3, self.read_pose)
        self.object = rospy.Subscriber('objects', Surrounding, self.callback_vision)
        self.collision = rospy.Subscriber('collision_output', Int16, self.collision)
        self.homingUpdate = rospy.Subscriber('homing_state', Int16, self.homing_update)
        self.command = rospy.Subscriber('search_in', Int16, self.command)
        
        
        
        self.px = 0.0
        self.py = 0.0
        self.pyaw = 90.0
        self.collided = False
        self.homing_state = 1
        
        self.threshold = 1
        self.count_moves = 0
        
        rate = rospy.Rate(1)
        
        while self.rel.get_num_connections() == 0:
            rospy.sleep(1)
        while not rospy.is_shutdown():
            #rospy.loginfo(self.state)
            if self.state == 1:
                rdist = random.random()+0.5
                ryaw = random.randint(-180,180)
                #rospy.loginfo('Going to random target...')
                self.rel.publish(Vector3(rdist, 0, ryaw))
                self.count_moves += 1
                self.wait_for_arrival(Vector3(rdist, 0, ryaw))
                if self.count_moves > self.threshold:
                    self.state = 0
                    self.output.publish(Int16(0))
                    

            
    def command(self, data):
        if self.state == 1 and data.data == 0:
            #stop moving
            self.rel.publish(Vector3(0,0,0))
        self.state = data.data
        if self.state == 1:
            self.count_moves = 0
            
    def wait_for_arrival(self, rel):
        ryaw = rel.z
        rdist = rel.x
        
        tyaw = ryaw + self.pyaw
        dx = rdist * math.sin(math.radians(tyaw))
        dy = -rdist * math.cos(math.radians(tyaw))
        tx = self.px + dx
        ty = self.py + dy
        while self.homing_state != 0 and self.collided == False:
            if self.state != 1:
                #new behavior
                #rospy.loginfo("new Command!")
                self.rel.publish(Vector3(0,0,0))
                return
            pass
            #rospy.loginfo(self.collided)
        if self.collided == True:
            rospy.loginfo('Collision detected.')
            while self.collided == True:
                pass
        else:    
            #rospy.loginfo('...arrived at random target.')
            self.homing_state = 1
        
    def read_pose(self, data):
        self.px = data.x
        self.py = data.y
        self.pyaw = data.z 
        
    def callback_vision(self, obj):
        if self.state == 1:
            if 'opponent' in obj.objects:
                self.state = 0
                self.output.publish(Int16(1))
            
    def collision(self, collision_data):
        if collision_data.data == 1:
            self.collided = True
        if collision_data.data == 0:
            self.collided = False
    
    def homing_update(self, homing_data):
        self.homing_state = homing_data.data

s = Search()


