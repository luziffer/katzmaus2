#!/usr/bin/env python

import os

import rospy
import math

from katzmaus.msg import Surrounding

from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64MultiArray, Int16
from nav_msgs.msg import Odometry
from geometry_msgs.msg._Vector3 import Vector3
from operation_mode import op_mode
from std_msgs.msg import String

class Controller:
    def __init__(self):
        rospy.init_node('controller', anonymous=True)
        
        self.commands = [rospy.Publisher('catch_in', Int16, queue_size = 10), 
                         rospy.Publisher('search_in', Int16, queue_size = 10),
                         rospy.Publisher('lurk_in', Int16, queue_size = 10)]
        
        rospy.Subscriber('finished', Int16, self.finished)
        rospy.Subscriber('start_stop', Int16, self.startstop)
        #for additional data if necessary
        #rospy.Subscriber('objects', Surrounding, self.callback_vision)
        
        #active behavior
        # -1: no behavior
        #  0: catch
        #  1: search
        #  2: lurk
        #TODO:
        #  3: curtain
        #  4: cheese
        self.active_strat = -1
        
        #variables that determine behavior
        self.last_strat = -1
        self.opponent_in_sight = False
        
        #for testing
        self.human_control = False
        
        
        while self.commands[0].get_num_connections() == 0 or self.commands[1].get_num_connections() == 0 or self.commands[2].get_num_connections() == 0:
            rospy.sleep(1)
        
        rospy.loginfo("Controller is ready. Use Startstop to start game")
        
        while not rospy.is_shutdown():
            if self.human_control:
                human = int(raw_input("0: catch, 1: search, 2: lurk: "))
                self.change_strat(human)
            else:
                pass
            
    def startstop(self, data):
        if data.data == -1:
            #stop all
            self.quit_all()
            self.last_strat = -1
        if data.data == 1:
            #reset and start first strat
            self.quit_all()
            self.last_strat = -1
            new_strat = self.evaluate_strat()
            self.change_strat(new_strat)
            
    def quit_all(self):
        for i in range(len(self.commands)):
            self.commands[i].publish(0) 
    
    def evaluate_strat(self):
        rospy.loginfo("evaluating")
        if self.opponent_in_sight:
            return 0  
        else:
            if self.last_strat == -1: #no last behavior --> start position
                return 1
            if self.last_strat == 2: #lurk
                return 1
            if self.last_strat == 0: #catch
                rospy.loginfo("last command was catch")
                return 2
            if self.last_strat == 1: #search
            
                #curtain
                #TODO
                return 2
            #TODO CURTAIN
        
        #no output
        raise RuntimeError("No correct behavior found")        

        
            
    def change_strat(self, new_strat):
        if new_strat == -1:
            #stop everything
            self.active_strat = -1
            self.last_strat = -1
            self.quit_all()
            return
        

        #switch off old strat
        self.quit_all()
        #turn on new strat
        rospy.loginfo("new Strat: " + str(new_strat))
        self.active_strat = new_strat
        self.commands[self.active_strat].publish(1)
    
    def finished(self, data):
        rospy.loginfo("finished")
        self.last_strat = self.active_strat
        #0: opponent not in sight
        #1: opponent is in sight
        if data.data == 0:
            self.opponent_in_sight = False
        if data.data == 1:
            if self.active_strat == 0:
                #TODO Stop controller
                pass
            self.opponent_in_sight = True
        self.quit_all()
        
        if not self.human_control:
            new_strat = self.evaluate_strat()
            self.change_strat(new_strat)
        
   
  
if __name__ == "__main__":
    c = Controller()
    
