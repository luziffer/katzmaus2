#!/usr/bin/env python

import os

import rospy

from std_msgs.msg import Int16


class StartStop:
    def __init__(self):
        rospy.init_node('startstop', anonymous=True)
        self.pub = rospy.Publisher('start_stop', Int16, queue_size = 10)
        while not rospy.is_shutdown():
            command = int(raw_input("-1: Stop Controller, 1: Start Controller\n>> "))
            self.pub.publish(Int16(command))
        
if __name__ == "__main__":
    s = StartStop()
    
