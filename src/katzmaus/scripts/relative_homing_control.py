#!/usr/bin/env python

import os

import rospy
import math
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64MultiArray
from nav_msgs.msg import Odometry
from geometry_msgs.msg._Vector3 import Vector3
from operation_mode import op_mode

class Control:
    def __init__(self):
        rospy.init_node("relative_homing_control", anonymous = True)
        #relative publishing
        self.rel = rospy.Publisher('rel_homing_command', Vector3, queue_size = 10)
        
        
        rate = rospy.Rate(1)
        while not rospy.is_shutdown():
            rdist = float(raw_input("relative Target dist? "))
            ryaw = float(raw_input("relative Target yaw? "))
            self.rel.publish(Vector3(rdist,0,ryaw))
            rate.sleep()
    
c = Control()
