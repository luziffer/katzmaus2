#!/usr/bin/env python

"""
Konzept: Der move_to Node bewegt den Roboter um einen vorgegebenen 2D-Vector und dreht dabei um einen bestimmten Winkel.

"""

import os

import rospy
import math
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from geometry_msgs.msg._Vector3 import Vector3
from operation_mode import op_mode


odom_path = ""
cmd_vel_path_input  = ""

if op_mode:
    #experiment:
    odom_path = "RosAria/pose"
    cmd_vel_path_input  = "cmd_vel"
     
else:
    odom_path = "p3dx/p3dx/base_pose_ground_truth"
    cmd_vel_path_input  = "swapped_cmd_vel"


class Movement:
    def __init__(self):
        rospy.init_node('move_to', anonymous=True)
        
        
        #listens for move_to controls of human:
        rospy.Subscriber("move_to_command", Vector3, self.callback_command)
        
        self.pub = rospy.Publisher('move_to_cmd_vel', Twist, queue_size = 10)
        rospy.Subscriber(cmd_vel_path_input, Twist, self.callback_key)
        self.odom = rospy.Subscriber(odom_path, Odometry, self.read_odom)
        
        self.absPose = rospy.Publisher('abs_pose', Vector3, queue_size = 10)
        
        self.printTimer = 0
        self.state = 0
        #state = 0: normal mode, accepts Keyboard input
        #state = 1: move_to mode, rotates to target
        #state = 2: move_to mode, drives to target
        #state = 3: move_to mode, rotates to final position
        
        self.tx = 0.0
        self.ty = 0.0  
        self.to = 0.0
             
        self.x = 0.0
        self.y = 0.0
        self.o = 0.0
        self.z = 0.0
        self.w = 0.0
        
        self.t = 0
        
        rate = rospy.Rate(25) # 25hz
        while not rospy.is_shutdown():
        
            self.dx = self.tx - self.x
            self.dy = self.ty - self.y
            
            if self.state == 1:

                if self.dx == 0.0 and self.dy > 0.0:
                    self.do = 180.0
                elif self.dx == 0.0 and self.dy < 0.0:
                    self.do = 0.0
                else:
                    self.do = (math.degrees(math.atan2(self.dy, self.dx)) + 90.0) % 360.0
                
                if (self.do - self.o) % 360.0 > 180.0:
                    vel_cmd = Twist(Vector3(0.0,0.0,0.0), Vector3(0.0,0.0,0.5))
                else:
                    vel_cmd = Twist(Vector3(0.0,0.0,0.0), Vector3(0.0,0.0,-0.5))
                if op_mode:
                    #experiment: controls are switched!
                    vel_cmd.angular.z *= -1
                self.pub.publish(vel_cmd)
                
                if math.fabs(self.do - self.o) < 3.0:
                    self.state = 2
                
            if self.state == 2:
                self.d = math.sqrt(self.dx * self.dx + self.dy * self.dy)
                vel_cmd = Twist(Vector3(0.5,0.0,0.0), Vector3(0.0,0.0,0.0))
                self.pub.publish(vel_cmd)
                if self.d < 0.1:
                    self.state = 3
                
                if self.dx == 0.0 and self.dy > 0.0:
                    self.do = 180.0
                elif self.dx == 0.0 and self.dy < 0.0:
                    self.do = 0.0
                else:
                    self.do = (math.degrees(math.atan2(self.dy, self.dx)) + 90.0) % 360.0
                if math.fabs(self.do - self.o) > 3.0 and self.t % 50 == 0:
                    self.state = 1                
                
            if self.state == 3:

                if (self.to - self.o) % 360.0 > 180.0:
                    vel_cmd = Twist(Vector3(0.0,0.0,0.0), Vector3(0.0,0.0,0.5))
                else:
                    vel_cmd = Twist(Vector3(0.0,0.0,0.0), Vector3(0.0,0.0,-0.5))
                if op_mode:
                    #experiment
                    vel_cmd.angular.z *= -1
                self.pub.publish(vel_cmd)
                
                if math.fabs(self.to - self.o) < 3.0:
                    self.state = 0
            
            self.t += 1
                
            
            rate.sleep()


    def callback_key(self, data):
        #only pass the keyboard data if we are in normal mode.
        if self.state == 0: 
            self.pub.publish(data)
        else:
            self.data = data

    def callback_command(self, data):
        #changes self.state to 1 and updates target
        target = data
        self.tx = target.x
        self.ty = target.y
        self.to = target.z
        self.state = 1

    def read_odom(self, data):
        pose = data.pose.pose
        self.x = pose.position.x
        self.y = pose.position.y
        
        self.z = pose.orientation.z
        self.w = pose.orientation.w
        if self.z > 0:
            self.o = (2.0 * math.degrees(math.acos(self.w)) + 90.0) % 360.0
        elif self.z < 0:
            self.o = (2.0 * math.degrees(math.acos(-1.0 * self.w)) + 90.0) % 360.0
        else:
            self.o = 90.0
        
        self.printTimer = (self.printTimer +1)%5
        if self.printTimer == 0:
            #(_,_,yaw) = euler_from_quaternion((pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w))
            info = "\nPos: %s \nQuat: %s\n%.3f\nState: %i" % ( str(pose.position), str(pose.orientation) , self.o, self.state)
            #rospy.loginfo(info)
            os.system('clear')
            rospy.loginfo("yaw:" + str(self.o))    
            rospy.loginfo("x:" + str(pose.position.x))
            rospy.loginfo("y:" + str(pose.position.y))
            rospy.loginfo("State:" + str(self.state))
            if self.state > 0:
                rospy.loginfo("diff:" + str(self.do - self.o))
            rospy.loginfo("----------------")
        
        self.absPose.publish(Vector3(self.x, self.y, self.o))
              

if __name__ == '__main__':
    move = Movement()
