#!/usr/bin/env python

import os

import rospy
import math
import random

from katzmaus.msg import Surrounding

from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64MultiArray, Int16
from nav_msgs.msg import Odometry
from geometry_msgs.msg._Vector3 import Vector3
from operation_mode import op_mode
from std_msgs.msg import String

class Lurk:
    def __init__(self):
        rospy.init_node('lurk', anonymous=True)
        
        self.state = 0
        #listens for vision:
        rospy.Subscriber("objects", Surrounding, self.callback_vision)
        
        #listens for homing
        rospy.Subscriber('homing_state', Int16, self.callback_homing)
        
        #collision
        rospy.Subscriber('collision_output', Int16, self.collision)
        
        #output of node
        self.output = rospy.Publisher('finished', Int16, queue_size = 10)
        
        self.rel_move = rospy.Publisher('rel_homing_command', Vector3, queue_size=10)
        self.slow_turn = rospy.Publisher("slow_turn", Int16, queue_size=10)

        
        self.count_commands = 0
        self.threshold = 5
        self.sign = 1
        if random.random() < 0.5:
            self.sign = -1
        
        while self.rel_move.get_num_connections() == 0 and self.output.get_num_connections() == 0: 
            rospy.sleep(1)


        self.command = rospy.Subscriber('lurk_in', Int16, self.command)
        
        rate = rospy.Rate(25) # 25hz
        while not rospy.is_shutdown():
            pass
            
            rate.sleep()
      
    def callback_homing(self, data):
        if self.state == 1:
            self.slow_turn.publish(1)
            if data.data == 0:
                self.rel_move.publish(Vector3(0,0,60 * self.sign))
                self.count_commands += 1
                if self.count_commands > self.threshold:
                    #finished with time out
                    self.count_commands = 0
                    self.output.publish(Int16(0))
      
    def command(self, data):
        if self.state == 1 and data.data == 0:
            #stop moving
            self.rel_move.publish(Vector3(0,0,0))
        self.state = data.data
        if self.state == 1:
            if random.random() < 0.5:
                self.sign = -1
            else:
                self.sign = 1

            self.count_commands = 0
            
      
    def callback_vision(self, obj):
        if self.state == 1:
            if 'opponent' in obj.objects:
                #finished with opponent detection
                self.output.publish(Int16(1))
                
               
    
    def collision(self, collision_data):
        if self.state == 1:
            if collision_data.data == 1:
                self.count_commands = 0
                self.output.publish(Int16(0))
            
            
if __name__ == "__main__":
    c = Lurk()
