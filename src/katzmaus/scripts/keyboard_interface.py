#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Konzept: Das teleop_keyboard Node gibt vertauschte Werte für links und rechts an. Dieses Node switcht das Vorzeichen von angular.z, so dass
         der Roboter richtig rum faehrt

"""


import rospy
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan


class KeyboardInterface():

    def __init__(self):
        #init
        rospy.init_node('keyboard_interface', anonymous=True)
        self.pub = rospy.Publisher('swapped_cmd_vel', Twist, queue_size = 10)
        rospy.Subscriber('cmd_vel', Twist, self.callback_key)
        self.otherpub = rospy.Publisher('p3dx2/p3dx_velocity_controller/cmd_vel', Twist, queue_size = 10)
        rospy.Subscriber('other_cmd_vel', Twist, self.callback_other_key)
        #listening
        self.listener()

    def callback_key(self, data):
        #Change the sign of the angular velocity
        data.angular.z *= -1
        self.pub.publish(data)

    def callback_other_key(self, data):
        #Change the sign of the angular velocity
        data.angular.z *= -1
        self.otherpub.publish(data)


    def listener(self):
        #just keep listening
        rospy.spin()
    
if __name__ == '__main__':
    ki = KeyboardInterface()
    
    
