#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Fangen.
"""

import rospy
from std_msgs.msg import String, Int16 
from geometry_msgs.msg import Vector3
from katzmaus.msg import Surrounding


class Catcher:
    def __init__(self):
        self.state = 0
        self.opp_last_frame = False
        rospy.init_node('catch', anonymous=True) 
        self.output = rospy.Publisher('finished', Int16, queue_size=10)
        self.input  = rospy.Subscriber('/objects', Surrounding, self.callback)
        self.command = rospy.Subscriber('catch_in', Int16, self.command)
        self.homing = rospy.Publisher('/rel_homing_command', Vector3, queue_size=10)
        self.slow_turn = rospy.Publisher('slow_turn', Int16, queue_size=10)
      
        rospy.spin()
    
    def command(self, data):
        if self.state == 1 and data.data == 0:
            #stop moving
            self.homing.publish(Vector3(0,0,0))
            
        self.state = data.data
            
        
    def callback(self, obj):
        #rospy.loginfo(self.state)
        if self.state == 1:
            if 'opponent' in obj.objects:
                self.opp_last_frame = True
                opponent_id = obj.objects.index('opponent')
                opponent_tri = obj.triangles[opponent_id]
                
                #if other robot is close enough it counts as caught
                if opponent_tri.dist < .05:
                    self.state = 0
                    self.output.publish(Int16(1))

                #move towards 
                self.slow_turn.publish(1)
                yaw = (opponent_tri.yaw_min + opponent_tri.yaw_max) / 2
                self.homing.publish(Vector3(1, 0, -yaw))
                rospy.loginfo("catching")
                #rospy.loginfo('yaw to opponent is %.3f' % yaw)
                #if abs(yaw) < 1:
                #    movement = Vector3(1.5, 0, 0)
                #    #movement = Vector3(0, 0, 0.1)
                #    rospy.loginfo('Moving forward')
                #else:
                #    movement = Vector3(0, 0, yaw/2)
                #    #movement = Vector3(yaw, 0, 0)
                #    rospy.loginfo('Moving by yaw=%f' % (yaw/2))
                #self.homing.publish(movement)
            else:
                #TODO: if it looses him for one frame thats ok
                if self.opp_last_frame == False:
                    self.state = 0
                    self.output.publish(self.output.publish(Int16(0)))
                self.opp_last_frame = False


if __name__ == '__main__':
    _ = Catcher()
