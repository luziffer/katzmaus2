#!/usr/bin/env python

"""
Konzept: Der move_to Node bewegt den Roboter um einen vorgegebenen 2D-Vector und dreht dabei um einen bestimmten Winkel.

#all calculations are done in radians

"""

import os

import rospy
import math
import numpy as np
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from geometry_msgs.msg._Vector3 import Vector3
from operation_mode import op_mode
from std_msgs.msg import String, Int16


odom_path = ""
cmd_vel_path_input  = ""

if op_mode:
    #experiment:
    odom_path = "RosAria/pose"
    cmd_vel_path_input  = "cmd_vel"
     
else:
    odom_path = "p3dx/p3dx/base_pose_ground_truth"
    cmd_vel_path_input  = "swapped_cmd_vel"


class Movement:
    def __init__(self):
        rospy.init_node('homing', anonymous=True)
        
        
        #listens for move_to controls of human:
        rospy.Subscriber("homing_command", Vector3, self.callback_command)
        rospy.Subscriber("slow_turn", Int16, self.callback_slow_turn)
        
        self.pub = rospy.Publisher('move_to_cmd_vel', Twist, queue_size = 10)
        rospy.Subscriber(cmd_vel_path_input, Twist, self.callback_key)
        
        self.odom = rospy.Subscriber(odom_path, Odometry, self.read_odom)
        self.stateUpdate = rospy.Publisher('homing_state', Int16, queue_size=10)
        
        self.absPose = rospy.Publisher('abs_pose', Vector3, queue_size = 10)
        
        
        self.tx = 0
        self.ty = 0  
        self.to = 0
             
        self.x = 500.0
        self.y = 0.0
        self.o = 0.0
        self.z = 0.0
        self.w = 0.0
        
        self.rho   = 0.0        
        self.alpha = 0.0
        self.phi   = 0.0
        
        self.t = 0
        
        self.state = 0
        self.slow_turn = False
        
        self.k_rho0 = 0.8
        self.k_alpha0 = 5
        self.k_phi0 = -2
        
        self.k_rho = 0
        self.k_alpha = 0
        self.k_phi = 0
        
        self.sigma = 2
        
        self.K = np.array([[self.k_rho, 0, 0], [0, self.k_alpha, self.k_phi]])
        
        rate = rospy.Rate(25) # 25hz
        while not rospy.is_shutdown():
            self.stateUpdate.publish(self.state)
            self.dx = self.tx - self.x
            self.dy = self.ty - self.y
            self.do = (self.to - self.o)%(2*math.pi)
            
            if self.do > math.pi:
                self.do = self.do - 2*math.pi
            
            #calculate the coords of dx and dy in the rotated frame
            R = np.array([[math.cos(self.o), math.sin(self.o)], [-math.sin(self.o), math.cos(self.o)]])
            beta = np.dot(R, np.array([self.dx, self.dy]))
            self.dx = beta[0]
            self.dy = beta[1]    
            
            #dx,dy,do are coords in cartesian space
            #convert to polar coords
            self.rho = math.sqrt(self.dx*self.dx + self.dy*self.dy)
            if self.rho > 1:
                self.rho = 1
            if self.dy == 0:
                if self.dx < 0:
                    self.alpha = -math.pi/2
                if self.dx == 0:
                    self.alpha = 0
                else:
                    self.alpha = math.pi/2
            else:
                self.alpha = math.atan(self.dx/self.dy)
            if self.dy < 0:
                if self.dx > 0:
                    self.alpha = math.pi + self.alpha
                else:
                    self.alpha = -math.pi + self.alpha

            self.alpha *= -1
            if op_mode:
                self.alpha *= -1
            #phi is the same for both coord systems
            self.phi =  self.do
            
            v = np.array([self.rho, self.alpha, self.phi])
            
            #tune K
            exponent = -(self.rho/self.sigma)**2
            self.k_rho = self.k_rho0*math.cos(self.alpha)*math.cos(self.alpha)
            self.k_alpha = self.k_alpha0*(1+math.sin(self.alpha)**2)*(2-math.exp(exponent))
            self.k_phi = self.k_phi0*(1+math.sin(self.alpha)**2)*math.exp(exponent)
            self.K = np.array([[self.k_rho, 0, 0], [0, self.k_alpha, self.k_phi]])
            
            #velocity for control
            u = np.dot(self.K, v)/4
            u[0] = u[0]*5
            if op_mode:
                if u[0] > 1:
                    u[0] = 1
            else:
                if u[0] > 1:
                    u[0] = 1
            if self.slow_turn:
                threshold = 0.4
            else:
                threshold = 0.8
            if abs(u[1]) > threshold:
                u[1] = np.sign(u[1])*threshold 
            
            if self.rho < 0.01:
                u[0] = 0 
            

            
               
            
            #publish the control vector
            vel_cmd = Twist(Vector3(u[0],0.0,0.0), Vector3(0.0,0.0,-u[1]))
            
            if self.state == 1:
                pass
                self.pub.publish(vel_cmd)
            
            if self.state == 2:
                if abs(self.phi) < 0.1:
                    #finished
                    self.slow_turn = False    
                    self.state = 0
                if self.slow_turn:
                    rotation = 0.8
                else:
                    rotation = 0.2
                if abs(self.phi) < 0.3:
                    rotation = 0.2
                if op_mode:
                    #experiment:
                    rotation *= -1
                if self.phi > 0:
                    
                    vel_cmd = Twist(Vector3(0.0,0.0,0.0), Vector3(0.0,0.0,-rotation))
                else:
                    vel_cmd = Twist(Vector3(0.0,0.0,0.0), Vector3(0.0,0.0,rotation))
                self.pub.publish(vel_cmd)
                
            
            #rospy.loginfo("vel_cmd: " + str(vel_cmd))
            #rospy.loginfo("phi: " + str(self.phi))fse
            #rospy.loginfo("state: " + str(self.state) + "|||     " + str(self.phi) + " " + str(self.alpha))
            #rospy.loginfo("pos: " + str([self.dx, self.dy, self.do]))
            #rospy.loginfo("-------------------")
            
            #check if goal is reached
            if self.rho < 0.1 and self.state == 1:
                self.state = 2
            
            self.t += 1
            rate.sleep()
            


    def callback_key(self, data):
    
        self.pub.publish(data)
    
        #only pass the keyboard data if we are in normal mode.
        if self.state == 0: 
            self.pub.publish(data)
        else:
            self.data = data

    def callback_command(self, data):
        #changes self.state to 1 and updates target
        target = data
        self.tx = target.x
        self.ty = target.y
        self.to = target.z
        self.state = 1
        
    def callback_slow_turn(self, data):
        if data.data == 1:
            #enable slow turn:
            self.slow_turn = True
        if data.data == 0:
            self.slow_turn = False
            

    def read_odom(self, data):
        pose = data.pose.pose
        self.x = pose.position.x
        self.y = pose.position.y
        
        self.z = pose.orientation.z
        self.w = pose.orientation.w
        if self.z > 0:
            self.o = (2.0 * math.degrees(math.acos(self.w)) + 90.0) % 360.0
        elif self.z < 0:
            self.o = (2.0 * math.degrees(math.acos(-1.0 * self.w)) + 90.0) % 360.0
        else:
            self.o = 90.0
        self.o = math.radians((self.o + 180)%360)
        
        self.absPose.publish(Vector3(self.x, self.y, self.o))
        #rospy.loginfo("absPose " + str(Vector3(self.x, self.y, self.o)))

if __name__ == '__main__':
    move = Movement()
