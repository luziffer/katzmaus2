#!/usr/bin/env python

"""
Konzept: Der 'vision' Node ist fuer die Detektion von farblich gekennzeichneten Objekten verantwortlich.

"""

import rospy
import math
import numpy as np
import math 
import cv2

import pdb 
import matplotlib.pyplot as plt

#from std_msgs.msg import String
#from geometry_msgs.msg import Vector3
from sensor_msgs.msg import Image

from katzmaus.msg import Triangle
from katzmaus.msg import Surrounding

from operation_mode import op_mode

#semantic definitions, 
# ordered by importance!
classes = np.array([
    'wall', 
    'blue_curtain',
    'orange_curtain',
    'cheese',  
    'opponent', 
])
colors = np.array([
    [128, 128, 128],
    [  0, 255, 244],
    [200, 136,  44],   
    [ 40, 100,  13],
    [255,   0,   0],
], dtype='uint8')


if not op_mode: # simulation
    none = dict(
        min_hue=137,
        max_hue=139,
        min_sat=138,
        max_sat=140,
        min_val=139,
        max_val=141,
        erosion_size=0,
        height_cuttoff=438,
        quantile_percent=1,
    )
    detection = [
        none, #wall is not important 
        none, # no blue curtain
        dict(# orange curtain
            min_hue=6,
            max_hue=26,
            min_sat=78,
            max_sat=255,
            min_val=80,
            max_val=187,
            erosion_size=3,
            height_cuttoff=100,
            quantile_percent=160,
        ),
        none, # no cheese
        dict( #opponent
            min_hue=0,
            max_hue=5,
            min_sat=87,
            max_sat=255,
            min_val=58,
            max_val=249,
            erosion_size=0,
            height_cuttoff=100,
            quantile_percent=150,
        ),
    ]
else: #experiment
    detection = [
        dict( #wall
            min_hue=25,
            max_hue=231,
            min_sat=19,
            max_sat=138,
            min_val=96,
            max_val=235,
            erosion_size=3,
            height_cuttoff=100,
            quantile_percent=100,
        ),
        dict( #blue_curtain 
            min_hue=0,
            max_hue=0,
            min_sat=0,
            max_sat=0,
            min_val=0,
            max_val=0,
            erosion_size=0,
            height_cuttoff=103,
            quantile_percent=160,
        ),
        dict( #orange_curtain
            min_hue=0,
            max_hue=0,
            min_sat=0,
            max_sat=0,
            min_val=0,
            max_val=0,
            erosion_size=0,
            height_cuttoff=126,
            quantile_percent=160,
        ),
        dict( #cheese
            min_hue=38,
            max_hue=58,
            min_sat=61,
            max_sat=235,
            min_val=19,
            max_val=220,
            erosion_size=3,
            height_cuttoff=160,
        ),
        dict( #opponent 
            min_hue=0,
            max_hue=12,
            min_sat=76,
            max_sat=235,
            min_val=13,
            max_val=220,
            erosion_size=5,
            height_cuttoff=160,
        ),
    ]


#size constants
robot_width = .42
cheese_width = .29

#additional noise reduction
detection_threshold = 0

#display triangle 
text_output = True


efficient = False

#camera constants
if not op_mode:
    camera = dict(
        rate = 30.0,         # frame rate in Hz  
        fov  = 1.3962634,    # horizontal field of view in rad
        near = .02,          # gives focal length of camera  
        path = 'p3dx/p3dx/camera1/image_raw'
    )
else: 
    camera = dict(
        rate = 30.0,         # frame rate in Hz  
        fov  = 1.014,         # horizontal field of view in rad
        near = .02,          # gives focal length of camera  
        path = 'usb_cam/image_raw'
    )        



class Vision:

    def __init__(self):
        
        if efficient:
            global classes
            global detection
            global colors
            classes = [classes[0], classes[-1]]
            detection = [detection[0], detection[-1]]
            colors = np.array([colors[0, :], colors[-1, :]], dtype='uint8')          
        
        
        rospy.init_node('vision', anonymous=True)
        
        # register image topic
        self.eyes = rospy.Subscriber(camera['path'], Image, self.see)
            
        # publish semantics and relative triangular distances of surrounding objects          
        self.surrounding = rospy.Publisher('objects', Surrounding, queue_size=1)

        # (optional) publish image with colour encoding of pixel labelling
        self.viewer = rospy.Publisher('objviewer', Image, queue_size=1)
             
        self.counter = 0
        self.threshold = 2

        rospy.spin()
    

    def see(self, image):
        #self.counter = (self.counter + 1) % self.threshold
        #if self.counter % self.threshold != 0: return
        
        # convert image to numpy tensor    
        w,h = image.width, image.height
        c = len( image.data ) / (image.width * image.height)
        
        #rgbdata = np.zeros((w,w,c),dtype='uint8')
        #rgbdata[:,:h,:] = np.fromstring( image.data , dtype='uint8' ).reshape( (w,h,c) )
        rgbdata = np.fromstring( image.data , dtype='uint8' )
        rgbdata = rgbdata.reshape( (h,w,c) )
        #rgbdata = rgbdata.transpose ( (1,0,2) )  
        
#        plt.imshow(rgbdata)
#        plt.show()
#        pdb.set_trace()
        
        #rospy.loginfo(rgbdata.shape)
        #h=w
        
        #rgbdata = rgbdata.transpose((1,0,2))
        #rospy.loginfo(rgbdata.shape)
        #rgbdata = np.array(rgbdata, dtype='uint8')
        
        # classification pipeline
        labels = np.zeros((h,w), dtype='uint8')
        boundings = [(0,1,0,1)] * len(classes)
        semantics = [0] * len(classes)
        for i,values  in enumerate(detection[1:]):
            i = i+1
            
            hsvdata = rgbdata.copy()
            cv2.rectangle(hsvdata, (0,0), (w-1,values['height_cuttoff']), (255,255,255), thickness=-1)
            hsvdata = cv2.cvtColor(hsvdata, cv2.COLOR_RGB2HSV)
        
            
            #historgram normalization, HSV bounds and height screening
            lower = np.array([ values['min_hue'], values['min_sat'], values['min_val']], dtype='uint8')
            upper = np.array([ values['max_hue'], values['max_sat'], values['max_val']], dtype='uint8')
            #rgbdata[:,:,0] = cv2.equalizeHist(image[:,:,0])
            #rgbdata[:,:,1] = cv2.equalizeHist(image[:,:,1])
            #rgbdata[:,:,2] = cv2.equalizeHist(image[:,:,2])
            #rgbdata = cv2.rectangle(rgbdata, (0,0), (w-1,values['height_cuttoff']), (255,255,255), thickness=-1)
            mask = cv2.inRange(hsvdata, lower,upper)
            
            #Erosion
#            for e in range(values['erosion_size']):
#                pass
#                if np.max(mask) > 0:
#                    w,h = mask.shape
#                    box = np.zeros((w+2, h+2, 9), dtype=bool)
#                    box[1:w+1,1:h+1,4] = (mask > 0)
#                    for l in [k for k in range(9) if k != 4]:
#                        a = l % 3 - 1
#                        b = l // 3 - 1
#                        box[1:w+1,1:h+1,l] = \
#                            box[1+a:w+1+a, 1+b:h+1+b, 4] 
#                    box  = box[1:w+1, 1:h+1, :]
#                    box = box.sum(axis=2) / 9   
#                    mask = 255 * (box > .5)
#            mask = np.array(mask, dtype='uint8')
            kernel = np.ones( (values['erosion_size'],values['erosion_size']) )
            kernel = kernel / np.sum(kernel)
            mask = cv2.erode(mask, kernel)
            
            #Tracking only if object was detected 
            total_detection = np.sum(mask > 0)
            
            semantics[i] = (total_detection > detection_threshold)
            
            if semantics[i]:
                
                ##calculate Gaussian parameters
                #x = np.repeat(np.arange(w), h).reshape(w,h)
                #y = np.repeat(np.arange(h), w).reshape(h,w).T
                #mx = np.sum( mask * x ) / total_detection
                #my = np.sum( mask * y ) / total_detection
                #sx = np.sum( mask * (x-mx)**2 ) / total_detection
                #sy = np.sum( mask * (y-my)**2 ) / total_detection
                #sx = np.sqrt(sx + 1e-6)
                #sy = np.sqrt(sy + 1e-6)
                
            
                #remove everything outside of quantile
                #quantile = values['quantile_percent']/100.
                
                
                ##sample bounding box
                #l = max(0, min(w-1, int(round(mx-quantile*sx)) ))
                #r = max(0, min(w-1, int(round(mx+quantile*sx)) ))
                #d = max(0, min(h-1, int(round(my*quantile*sy)) ))
                #u = max(0, min(h-1, int(round(my-quantile*sy)) ))
            
                ##crop new image
                #mask[:l,:] = 0
                #mask[r+1:,:] = 0
                #mask[:,d+1:] = 0
                #mask[:,:u] = 0
                
                #fit bounding box
                l = np.max(mask, axis=1).argmax() - values['erosion_size']
                r = mask.shape[0]-1 - np.max(mask, axis=1)[::-1].argmax() + values['erosion_size']
                u = np.max(mask, axis=0).argmax() - values['erosion_size']
                d = mask.shape[1]-1 - np.max(mask, axis=0)[::-1].argmax() + values['erosion_size']
                boundings[i] = (l,r,u,d)
                
            
                #labels[l:r+1,u:d+1] = i
                labels = np.where( mask > 0 , i, labels)
        
        #create colour encoded pixel labelling, only useful for debugging
        view = colors[labels]
        
        #view = rgbdata
        for i,color in enumerate(colors):
            l,r,u,d = boundings[i]
            color = tuple(color)

            view = cv2.transpose(view)
            if not efficient:
                cv2.rectangle(view, (l,u), (r,d), 0 )
            else:
                cv2.rectangle(view, (l,u), (r,d), (255,0,0), thickness=-1 )
            
            view = cv2.transpose(view)
        
        surrounding = Surrounding()
        
        for i in range( len(classes) ):
            if semantics[i]:
                triangle =  Triangle()    
                l,r,u,d = boundings[i]
                triangle.yaw_min = np.degrees((2.0 * u/(h-1) - 1.0) * camera['fov']/2 )
                triangle.yaw_max = np.degrees((2.0 * d/(h-1) - 1.0) * camera['fov']/2 )
                
                #if not op_mode:
                if not efficient:
                    triangle.dist = 5 * robot_width / ( math.tan(camera['fov']) * (d - u)/(w-1) )
                else:
                    triangle.dist = 1
                    
                if text_output and classes[i] != 'wall':
                    text = classes[i] + ("\nd: %.3f\ny: %.3f\nY: %.3f" %  (triangle.dist, triangle.yaw_min, triangle.yaw_max))
                    lines = text.split('\n')
                    for t, line in enumerate(lines):    
                        view = cv2.putText(view, line, (u, l-15*(len(lines) - t) ), cv2.FONT_HERSHEY_SIMPLEX, .5, (0, 0, 0))        
                            
                surrounding.objects.append(classes[i])
                surrounding.triangles.append(triangle)
                
        
        #publish segmantation
        #view = view[:480,:640,:].tostring()
        view = view.tostring(order='C')
        image.data = view
        self.viewer.publish( image )
        
        #publish semantics
        self.surrounding.publish(surrounding) 
       


if __name__ == '__main__':
    vision = Vision()
