#!/usr/bin/env python

import os

import rospy
import math
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64MultiArray
from nav_msgs.msg import Odometry
from geometry_msgs.msg._Vector3 import Vector3
from operation_mode import op_mode

class Control:
    def __init__(self):
        rospy.init_node("move_to_control", anonymous = True)
        self.pub = rospy.Publisher('move_to_command', Vector3, queue_size = 10)
        
        rate = rospy.Rate(1)
        while not rospy.is_shutdown():
            self.x = float(raw_input("Target x-Koordinate? "))
            self.y = float(raw_input("Target y-Koordinate? "))
            self.yaw = float(raw_input("Target yaw? "))
            self.pub.publish(Vector3(self.x ,self.y, self.yaw))
            rate.sleep()

c = Control()
