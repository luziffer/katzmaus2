#! /usr/bin/env python

import numpy as np
import cv2 
import sys

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QVBoxLayout, QLabel, QSlider, QPushButton, QFileDialog
from PyQt5.QtGui import QImage, QPixmap

from collections import OrderedDict


class Viewer( QWidget ):
    def __init__(self):
        super(QWidget, self).__init__()
        self.interface = QVBoxLayout(self)
        
        #Create sliders
        self.sliders = OrderedDict()
        for component in ['min_hue', 'max_hue', 'min_sat', 'max_sat', 'min_val', 'max_val',
                          'erosion_size', 'height_cuttoff']:
                
                label = QLabel(component)
                slider = QSlider(Qt.Horizontal, self)
                if component == 'height_cuttoff':
                    slider.setMinimum(0)
                    slider.setMaximum(500)
                    slider.setTickInterval(10)
                    slider.setValue(100)
                else:
                    slider.setMinimum(0)
                    slider.setMaximum(255)
                    slider.setTickInterval(1)
                    
                    if component.startswith('min'):
                        slider.setValue(0)
                    elif component.startswith('max'):
                        slider.setValue(255)
                
                slider.valueChanged.connect(self.update)
                self.sliders[component] = slider
                
                layout = QHBoxLayout()
                layout.addWidget(label)
                layout.addWidget(slider)
                self.interface.addLayout(layout)
        
        
        #Display values
        self.output = QLabel('< output >')
        self.interface.addWidget(self.output)
        
        #File operations
        fileops = QHBoxLayout()
        saver = QPushButton('Save Mask')
        saver.clicked.connect(self.storeimg)
        fileops.addWidget(saver)
        opener = QPushButton('Open image')
        opener.clicked.connect(self.loadimg)
        fileops.addWidget(opener)
        saver = QPushButton('Save Parameters')
        saver.clicked.connect(self.storetxt)
        fileops.addWidget(saver)
        text = QPushButton('Open Parameters')
        text.clicked.connect(self.loadtxt)
        fileops.addWidget(text)
        self.interface.addLayout(fileops)
        
        
        #Image viewer
        viewer = QHBoxLayout()
        self.original = QLabel('< original image >')
        self.masked  = QLabel('< image mask >')
        viewer.addWidget(self.original)
        viewer.addWidget(self.masked)
        self.interface.addLayout(viewer)
        
        
        #initialize picture
        self.img = np.zeros((2,2,3), dtype='uint8')
        self.update()
    
    #routine for reading values from widget
    def readvalues(self):
        values = OrderedDict()
        for component in self.sliders:
            values[component] = self.sliders[component].value()
        return values
        
    #Keep values up to date
    def update(self):
        values = self.readvalues()
        self.output.setText(', '.join([c+'=%d' % values[c] for c in values]))
        
        
        #historgram normalization, HSV bounds and height screening
        lower = np.array([ values['min_hue'], values['min_sat'], values['min_val']])
        upper = np.array([ values['max_hue'], values['max_sat'], values['max_val']])
        w,h = self.img.shape[1], self.img.shape[0]
        image = np.array(self.img, dtype='uint8') 
        #image[:,:,0] = cv2.equalizeHist(image[:,:,0])
        #image[:,:,1] = cv2.equalizeHist(image[:,:,1])
        #image[:,:,2] = cv2.equalizeHist(image[:,:,2])
        image = cv2.rectangle(image, (0,0), (w-1,values['height_cuttoff']), (255,255,255), thickness=-1)
        image = cv2.cvtColor(image, cv2.COLOR_RGB2HSV )
        mask = cv2.inRange(image, lower,upper)
        
        #Erosion
        for i in range(values['erosion_size']):
            if np.max(mask) > 0:
                w,h = mask.shape
                
                box = np.zeros((w+2, h+2, 9), dtype=bool)
                box[1:w+1,1:h+1,4] = (mask > 0)
                for l in [k for k in range(9) if k != 4]:
                    i = l % 3 - 1
                    j = l // 3 - 1
                    box[1+i:w+1+i,1+j:h+1+j,l] = \
                        box[1:w+1, 1:h+1, 4] 
                box  = box[1:w+1, 1:h+1, :]
                box = box.sum(axis=2) / 9
                mask = 255 * (box > .5)
        mask = np.array(mask, dtype='uint8')
        
        #bounding box
        l = np.max(mask, axis=1).argmax()
        r = mask.shape[0] - np.max(mask, axis=1)[::-1].argmax()
        u = np.max(mask, axis=0).argmax()
        d = mask.shape[1] - np.max(mask, axis=0)[::-1].argmax()
        
        #to rgb
        mask = np.repeat(mask[:,:,None], 3, axis=2)
        self.mask = mask.copy()
        
        #Display with bounding box
        mask = cv2.rectangle(mask, (d,l), (u,r),(255,0,0))
        image = np.transpose(mask,(0,1,2)).copy()
        image = QImage( image , image.shape[1], image.shape[0], QImage.Format_RGB888 )
        pixmap = QPixmap(image)
        self.masked.setPixmap(pixmap)
        
    
    #save image to file
    def storeimg(self):
        path, _ = QFileDialog.getSaveFileName(None, "Save image", ".","PNG images (*.png);;All Files (*)")
        cv2.imwrite(path, self.mask)
        
    #Load image from file
    def loadimg(self):
        path, _ = QFileDialog.getOpenFileName(self, "Open image", ".","PNG images (*.png);;All Files (*)")
        self.img  = cv2.imread(path)[:,:,::-1]
        
        image =  np.transpose(self.img,(0,1,2)).copy()
        image = QImage( image , image.shape[1], image.shape[0], QImage.Format_RGB888 )
        pixmap = QPixmap(image)
        self.original.setPixmap(pixmap)
        self.update()
    
    #save text data
    def storetxt(self):
        path, _ = QFileDialog.getSaveFileName(self, "Save values", ".","Text (*.txt);;All Files (*)")
        values = self.readvalues()
        txt = '\n'.join([component + "=" + str(values[component]) + "," for component in values])
        with open(path, "w") as f:
            f.write(txt)
    
    def loadtxt(self):
        path, _ = QFileDialog.getOpenFileName(self, "Open values", ".","Text (*.txt);;All Files (*)")
        with open(path, "r") as f:
            txt = f.read()
        values = eval('dict(%s)' % txt)
        for component in self.sliders:
            self.sliders[component].setValue(values[component])
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    viewer = Viewer()
    viewer.show()
    app.exec_()

