#! /usr/bin/env python

import numpy as np
import cv2 
import sys

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QVBoxLayout, QLabel, QSlider, QPushButton, QFileDialog
from PyQt5.QtGui import QImage, QPixmap

from collections import OrderedDict


class Viewer( QWidget ):
    def __init__(self):
        super(QWidget, self).__init__()
        self.interface = QVBoxLayout(self)
        
        #Create sliders
        self.sliders = OrderedDict()
        for component in ['quantile_percent']:
                
                label = QLabel(component)
                slider = QSlider(Qt.Horizontal, self)
                if 'percent' in component:
                    slider.setMinimum(0)
                    slider.setMaximum(300)
                    slider.setTickInterval(1)
                    
                if component == 'quantile_percent':
                    slider.setValue(75)
                
                slider.valueChanged.connect(self.update)
                self.sliders[component] = slider
                
                layout = QHBoxLayout()
                layout.addWidget(label)
                layout.addWidget(slider)
                self.interface.addLayout(layout)
        
        #File operations
        fileops = QHBoxLayout()
        saver = QPushButton('Save prediction')
        saver.clicked.connect(self.storeimg)
        fileops.addWidget(saver)
        opener = QPushButton('Open mask')
        opener.clicked.connect(self.loadimg)
        fileops.addWidget(opener)
        saver = QPushButton('Save Parameters')
        saver.clicked.connect(self.storetxt)
        fileops.addWidget(saver)
        text = QPushButton('Open Parameters')
        text.clicked.connect(self.loadtxt)
        fileops.addWidget(text)
        self.interface.addLayout(fileops)
        
        #Display values
        self.valuedisplay = QLabel('< values >')
        self.interface.addWidget(self.valuedisplay)
        
        
        #initialize picture
        self.mask = np.zeros((2,2), dtype='uint8')
        self.output = QLabel('< output >')
        self.interface.addWidget(self.output)
        self.update()
        
    #routine for reading values from widget
    def readvalues(self):
        values = OrderedDict()
        for component in self.sliders:
            values[component] = self.sliders[component].value()
        self.valuedisplay.setText(', '.join([c+'=%d' % values[c] for c in values]))    
        return values
    
    
    def update(self):
        values = self.readvalues()
        
        w,h = self.mask.shape
        
        total_detection = np.sum(self.mask)
        if total_detection > 0:
            image = self.mask.copy()
            
            #calculate Gaussian parameters
            x = np.repeat(np.arange(w), h).reshape(w,h)
            y = np.repeat(np.arange(h), w).reshape(h,w).T
            mx = np.sum( self.mask * x ) / total_detection
            my = np.sum( self.mask * y ) / total_detection
            sx = np.sum( self.mask * (x-mx)**2 ) / total_detection
            sy = np.sum( self.mask * (y-my)**2 ) / total_detection
            sx = np.sqrt(sx)
            sy = np.sqrt(sy)
            
            
            #remove everything outside of quantile
            quantile = values['quantile_percent']/100.
            
                
            #sample bounding box
            l = max(0, min(w-1, int(round(mx-quantile*sx)) ))
            r = max(0, min(w-1, int(round(mx+quantile*sx)) ))
            u = max(0, min(h-1, int(round(my-quantile*sy)) ))
            d = max(0, min(h-1, int(round(my+quantile*sy)) ))
        
            #crop new image
            image = image.copy()
            #image[:l,:] = 0
            #image[r+1:,:] = 0
            #image[:,d+1:] = 0
            #image[:,:u] = 0
            
            #fit bounding box
            #l = np.max(image, axis=1).argmax()
            #r = image.shape[0] - np.max(image, axis=1)[::-1].argmax()
            #u = np.max(image, axis=0).argmax()
            #d = image.shape[1] - np.max(image, axis=0)[::-1].argmax()
            
            #turn to rgb
            image = np.repeat(image[:,:,None],3,axis=-1)
            image = np.transpose(image, (0,1,2) )
            
            image = cv2.rectangle(image, (u,l), (d,r), (255,0,0))
            self.prediction = image
            image = QImage( image , h, w, QImage.Format_RGB888 )
            pixmap = QPixmap(image)
            self.output.setPixmap(pixmap)
        
    #save image to file
    def storeimg(self):
        path, _ = QFileDialog.getSaveFileName(None, "Save image", ".","PNG images (*.png);;All Files (*)")
        cv2.imwrite(path, self.prediction[:,:,::-1])
        
    #Load image from file
    def loadimg(self):
        path, _ = QFileDialog.getOpenFileName(self, "Open image", ".","PNG images (*.png);;All Files (*)")
        self.mask  = cv2.imread(path).mean(axis=2).astype('uint8')
        print('mask: ', self.mask.shape)
        self.update()
        
        
    #save text data
    def storetxt(self):
        path, _ = QFileDialog.getSaveFileName(self, "Save values", ".","Text (*.txt);;All Files (*)")
        values = self.readvalues()
        txt = '\n'.join([component + "=" + str(values[component]) + "," for component in values])
        with open(path, "w") as f:
            f.write(txt)
    
    def loadtxt(self):
        path, _ = QFileDialog.getOpenFileName(self, "Open values", ".","Text (*.txt);;All Files (*)")
        with open(path, "r") as f:
            txt = f.read()
        values = eval('dict(%s)' % txt)
        for component in self.sliders:
            self.sliders[component].setValue(values[component])

    
if __name__ == '__main__':
    app = QApplication(sys.argv)
    viewer = Viewer()
    viewer.show()
    app.exec_()

