# KatzMaus


Implements a Pioneer P3DX robot acting as a cat to catch a similar robot acting as a mouse. 

Authors: Philipp Dahlinger, Marvin Ruder, Luca Lenz

For eductational use only, in particular for the Robotic Games Project of Autonomous Mobile Robots Lab. 

Documentation for the software is provided in `documentation.pdf`.


## Dependencies 

Supports both ROS Melodic and Kinetic. Has been tested! 

Using Python 2.7 and OpenCV v2.


## Current usage instructions 

The ROS package name is `katzmaus`. Make sure you have build the ROS package and installed the ROS messages. 
The build script `make.py` is very helpful for this. 
(Note: We are running Python v2.7)

To test the simulation you can call the `autorun.py` utility. We had trouble opening multiple terminals from a single script.
Therefore 
(Alternative might have been a .launch file.)


To run any of the modules start new terminals and run 

```rosrun katzmaus [script] ```

where `script` is any node source file located in `src/katzmaus/scripts/*.py'

Have a look at the `Wettkampf Rosruns` for a precise order of the nodes that have to be called. 


To check whether all nodes are correct compare the current graph to the documentation. 
The graphical output can be found on the `/objviewer` topic.


The following commands might be useful

`rosrun rqt_graph rqt_graph`

`rosrun rqt_image_viewer rqt_image_viewer`


Have fun! 