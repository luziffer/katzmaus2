#!/usr/bin/python2
## Caveat! Incompatible with Anaconda!!

"""
Utility script for setting up and running the katzmaus project.

"""

#Caveat! Has to be run from bash terminal for output to work properly.
import os


    

def make():
    os.system("clear")
    title = '''
        ==============================
           
         katzmaus utility tool script 
         
        ==============================                           
    
----------------------------------------
 options: (all true recommended)
----------------------------------------
        
    '''
    print(title)
    
    
    choices = dict()
    choices['catkin'] = yesno('Remake with catkin? ')
    choices['bashrc'] = yesno('Override ~/.bashrc? ')
        
    if choices['catkin']:
        print('\n>> Running catkin make.')
        os.system("catkin_make")	
        print('\n>> Running catkin make installation.')
        os.system("catkin_make install")

    if choices['bashrc']:
        print('\n>> Appending bash settings.')
        append_bashrc()
        


#---- installation tasks -----
    
def get_workdir():
    wd = os.path.realpath(__file__)
    wd = os.path.dirname(wd)
    print( 'Project directory: ', wd )
    return wd
    
def append_bashrc():
    bashrc = "~/.bashrc"
    bashrc = os.path.expanduser(bashrc)
    wd = get_workdir()
    bashrc = os.path.relpath(bashrc, start=wd)
    with open(bashrc, 'a') as f:
        f.write('\nsource %s/devel/setup.sh\n' % wd)
    print('Written to: ', bashrc)

# ----------------------------



#---- interface -----
def yesno(msg):
    _ = "+--------------------------+\n"
    chosen = False
    msg = msg + ' [y/n] '
    reminder = _
    while not chosen:
        choice = raw_input(reminder + msg)
        reminder = " Please answer YES or NO.\n"
        if len(choice)>0:
            choice = choice.lower()[0]
            if choice=='y':
                choice = True
                chosen = True
            elif choice=='n':
                choice = False
                chosen = True
    print(_)
    return choice


#this is an installer script
if __name__ == '__main__':
    make()
